echo '----------------------------------------------------------------------'
# Load environment settings...
if [ -f /home/pi/settings ]; then
  while IFS="= " read var value; do
    [ ! -z $var ] && [ "${var:0:1}" != "#" ] && export $var="$value"
  done < /home/pi/settings
fi
MAMEROM=$(ps h -C mame -o cmd | awk '{print $2}'); [ "${MAMEROM:0:1}" == "-" ] && MAMEROM=
echo "Current Frontend: $(case $FRONTEND in \
  attract) echo "Attract Mode$([ -x /usr/local/bin/attract ] && attract -v | awk '/Attract-Mode/ {print " " $2}')" ;; \
  advance) echo AdvanceMENU ;; \
  mame) ([ ! -z $MAMEROM ] && [ "$MAMEROM" == "$AUTOROM" ]) && echo 'None/AutoROM Mode' || echo 'MAME GUI' ;; \
  *) echo 'NOT SET' ;; esac)."
[ ! -z $MAMEROM ] && echo Currently emulated ROM: \
  $(/home/pi/mame/mame -listfull $MAMEROM | awk -F '"' '!/Description:$/ {print $2}').
[ -x $HOME/scripts/versions-check.sh ] && . $HOME/scripts/versions-check.sh
echo "The system is currently in $(systemctl -q is-active mame-autostart.service && echo ARCADE || echo SERVICE) mode."

setterm --cursor on
