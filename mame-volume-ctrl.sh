#!/bin/bash
#file:/home/pi/scripts/mame-vol.sh

function cleanup {
  # Deactivate GPIO pin
  echo $GPIOPIN >/sys/class/gpio/unexport 2>/dev/null
  exit 0
  }

function waitStateChange {
  while [ $(cat /sys/class/gpio/gpio${GPIOPIN}/value) != $1 ]; do
    inotifywait -q -e modify /sys/class/gpio/gpio${GPIOPIN}/value > /dev/null
  done
  }

# Clean up when exit
trap cleanup EXIT
trap cleanup SIGHUP
trap cleanup SIGQUIT
trap cleanup SIGINT
trap cleanup SIGTERM

HIGH='95%'
MED='85%'
LOW='72%'
MUTE='0%'

GPIOPIN=4   # We use GPIO 4

# Initialize GPIO pin
[ -d /sys/class/gpio/gpio${GPIOPIN} ] && echo $GPIOPIN > /sys/class/gpio/unexport  # Deactivate GPIO pin
echo $GPIOPIN > /sys/class/gpio/export    # Activate GPIO pin
sleep 0.1                                 # A small delay is required so that the system has time
                                          # to properly create and set the file's permission
echo in > /sys/class/gpio/gpio${GPIOPIN}/direction # Input signal
echo both > /sys/class/gpio/gpio${GPIOPIN}/edge    # We use the interrupt controller to avoid a CPU spin loop

waitStateChange 1      # Wait for button release
while true; do
  waitStateChange 0    # Wait for button press
  # Volume button pressed
  CHECK_VOL=$(amixer | awk -F'[\[\]]' '/Mono: Playback/ {print $2}')

  case $CHECK_VOL in
    $HIGH)
      # Setting volume to medium
      amixer -q cset numid=1 $MED ;;
    $MED)
      # Setting volume to low
      amixer -q cset numid=1 $LOW ;;
    $LOW)
      # Setting volume to mute
      amixer -q cset numid=1 "$MUTE" ;;
    $MUTE)
      # Setting volume to high
      amixer -q cset numid=1 $HIGH ;;
    *)
      # Setting volume to medium (default)
      amixer -q cset numid=1 $MED ;;
  esac
    waitStateChange 1  # Wait for button release
done
